[![](https://discordapp.com/api/guilds/96753964485181440/widget.png?style=banner2)](https://discord.gg/ngNQjT5) [![](https://s12.directupload.net/images/200916/joj33k55.png)](https://twitter.com/kreezxil) [![](https://s12.directupload.net/images/200916/efhmdjhg.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.reddit.com%252fr%252fMinecraftModdedForge)

[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

Do you like buttons? Buttons with different textures? Buttons that are still buttons for redstone circuits but are also **BIG**?

![](https://media.forgecdn.net/attachments/216/410/2017-08-28_11.png)

This mod has the same 200+ buttons that my other mod [**More Beautiful Buttons**](https://minecraft.curseforge.com/projects/more-beautiful-buttons) has, only they now use the **BIGGER BLOCK** model from [OpenBlocks](https://minecraft.curseforge.com/projects/openblocks?gameCategorySlug=mc-mods&projectID=228816)!

To use the buttons click right in the center of them, this is going to be fixed shortly.

## Todo:

*   Increase the size of the Bounding Box to include all of the textured part of the now larger block model.
*   Import more needed classes from the OpenMods library.

## ModPacks

You don't need my permission. Add it and enjoy! While not required, a link back would be greatly appreciated.

## Contact Methods

Other than the obvious methods provided by Curse. You can also chat with me live on Discord and Twitch at the following. The servers are public.

Discord: [https://discord.gg/Nm9W3zw](https://discord.gg/Nm9W3zw)

Twitch: [https://invite.twitch.tv/v9NhmX](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252finvite.twitch.tv%252fv9NhmX)

## Help a Veteran today

I am Veteran of United States Army. I am not disabled. But I do love to make these mods and modpacks for you guys. Please help me to help you by Donating at [https://patreon.com/kreezxil&nbsp;](https://www.curseforge.com/minecraft/mc-mods/big-beautiful-buttons)[![](https://www.kreezcraft.com/wp-content/uploads/2018/01/patreon.png)](https://patreon.com/kreezxil).

**This project is proudly powered by FORGE**, without whom it would not be possible.Help FORGE get rid of Adfocus at [https://www.patreon.com/LexManos](https://www.patreon.com/LexManos).
